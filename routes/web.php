<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TaskController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/task/all', [TaskController::class, 'index']);
Route::post('/task/store', [TaskController::class, 'store']);
Route::post('/task/update/{id}', [TaskController::class, 'update']);
Route::put('/task/destroy/{id}', [TaskController::class, 'destroy']);

