<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;

    protected $table = 'usuario';
    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    public static function getAll()
    {
        return self::all();
    }

    public static function getById($id)
    {
        return self::findOrFail($id);
    }

    public static function createItem($data)
    {
        return self::create($data);
    }

    public static function updateItem($id, $data)
    {
        $item = self::findOrFail($id);
        $item->update($data);
        return $item;
    }

    public static function deleteItem($id)
    {
        $item = self::findOrFail($id);
        $item->delete();
    }
}
