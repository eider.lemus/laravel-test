<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';
    protected $fillable = [
        'name',
        'description',
        'status'
    ];

    public static function getAll()
    {
        return self::all();
    }

    public static function getById($id)
    {
        return self::findOrFail($id);
    }

    public static function createItem($data)
    {
        return self::create($data);
    }

    public static function updateItem($id, $data)
    {
        $item = self::findOrFail($id);
        $item->update($data);
        return $item;
    }

    public static function deleteItem($id)
    {
        $item = self::findOrFail($id);
        $item->delete();
    }
}
