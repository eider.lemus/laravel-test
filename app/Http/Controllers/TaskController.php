<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Muestra una lista de tareas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tareas = Task::all();
        return response()->json($tareas, 200);
    }

    /**
     * Almacena una nueva tarea en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task;
        $task->name = $request->input('name');
        $task->description = $request->input('description');
        $task->status = $request->input('status');
        $task->save();
        return response()->json($task, 201);
    }


    /**
     * Actualiza una tarea específico en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tarea = Task::findOrFail($id);
        $tarea->update($request->all());
        return response()->json($tarea, 200);
    }

    /**
     * Elimina una tarea específico de la base de datos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarea = Task::findOrFail($id);
        $tarea->delete();
        return response()->json(null, 204);
    }
}
